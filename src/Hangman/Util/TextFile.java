package Hangman.Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leho Kivistik on 22.01.2017.
 */
public class TextFile {

    String filename;

    public TextFile(String filename) {
        this.filename = filename;
    }

    public List<String> ReadAllLines() {

        List<String> allLines = new ArrayList<String>();

        try {
            File file = new File(filename);

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(file), "UTF8"));

            String str;

            while ((str = reader.readLine()) != null) {
                allLines.add(str);
            }

            reader.close();

        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return allLines;
    }
}