package Hangman;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import Hangman.Core.*;
import Hangman.Util.*;

public class Main extends Application {

    private final String eestiTahestik = "ABCDEFGHIJKLMNOPQRSŠZŽTUVWÕÄÖÜXY";

    int ELUSID = 7;

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Sõnaarvamismäng");

        List<String> read = new TextFile("resources//Maailmariigid.txt").ReadAllLines();
        List<Sona> sonad = new ArrayList<Sona>();
        for (String sona : read) {
            sonad.add(new Sona(sona, new Kategooria("Maailmariigid")));
        }

        ArvamisMang mang = new ArvamisMang(sonad, eestiTahestik);
        mang.alusta(ELUSID);

        // https://docs.oracle.com/javase/8/javafx/api/javafx/scene/layout/BorderPane.html
        BorderPane paigutus = new BorderPane();

        //Mängu juhtala
        Label manguSeis = new Label();
        manguSeis.setStyle("-fx-font-size: 18;");
        manguSeis.textProperty().bind(Bindings.createStringBinding(() ->
        {
            switch (mang.SonaArvamine().manguSeis().get()) {
                case 1:
                    return "Proovi edasi!";
                case 0:
                    return "Alustame...";
                case -1:
                    return "Mäng läbi!";
                case -2:
                    return "TUBLI!";
            }
            return "";
        }, mang.SonaArvamine().manguSeis()));

        HBox manguJuhtmimine = new HBox(manguSeis);
        manguJuhtmimine.setPadding(new Insets(5, 5, 5, 5));
        manguJuhtmimine.setAlignment(Pos.CENTER);
        manguJuhtmimine.setMinWidth(300);
        manguJuhtmimine.setMinHeight(100);

        //Arvatav (peidetud) sõna
        Label peidetudSona = new Label(mang == null ? "" : mang.SonaArvamine().getArvatavSona());
        peidetudSona.textProperty().bind(Bindings.createStringBinding(() -> {
                    return mang.SonaArvamine().peidetudSona();
                },
                mang.SonaArvamine().pakutudTahed(),
                mang.SonaArvamine().peidetudSonaProp()
        ));

        peidetudSona.setFont(new Font("sans-serif", 64));
        peidetudSona.setPadding(new Insets(12, 12, 12, 12));
        peidetudSona.setMinWidth(300);

        HBox peidetudSonaPaan = new HBox(peidetudSona);
        peidetudSonaPaan.setPadding(new Insets(10, 10, 10, 10));
        peidetudSonaPaan.setAlignment(Pos.CENTER);

        // Tähtede ruudustik
        TilePane taheNuppudePaan = new TilePane();
        taheNuppudePaan.setPadding(new Insets(15, 12, 15, 12));
        //nuppudele võrdsed vahed
        taheNuppudePaan.setHgap(3);
        taheNuppudePaan.setVgap(3);
        taheNuppudePaan.setMinWidth(300);
        taheNuppudePaan.setMinHeight(100);

        for (int i = 0; i < eestiTahestik.length(); i++) {
            Character chr = eestiTahestik.charAt(i);
            Button button = new Button((chr).toString());
            button.disableProperty().bind(Bindings.createBooleanBinding(
                    () -> mang.SonaArvamine().pakutudTahed().contains(chr),
                    mang.SonaArvamine().pakutudTahed()));
            button.setOnAction(e -> mang.SonaArvamine().pakuTaht(chr));
            taheNuppudePaan.getChildren().add(button);
        }

        //Elud
        HBox kaotatudEludePaan = new HBox();
        kaotatudEludePaan.setPadding(new Insets(50, 10, 50, 10));
        kaotatudEludePaan.setAlignment(Pos.CENTER_LEFT);

        for (int i = 0; i < mang.SonaArvamine().elusid(); i++) {
            Label kaotatudElu = new Label("X");
            kaotatudElu.setStyle("-fx-font-size: 36;");
            StackPane kaotatudEluKlots = new StackPane(kaotatudElu);
            kaotatudEluKlots.setStyle("-fx-background-color: -fx-color, black, -fx-color; -fx-background-insets: 0, 1, 2; -fx-min-width: 36; -fx-min-height: 36;");
            kaotatudEludePaan.getChildren().add(kaotatudEluKlots);
            kaotatudElu.visibleProperty().bind(
                    mang.SonaArvamine().valesidPakkumisiProp().greaterThan(i));
        }

        paigutus.setTop(peidetudSonaPaan);
        paigutus.setCenter(manguJuhtmimine);
        paigutus.setLeft(kaotatudEludePaan);
        paigutus.setRight(taheNuppudePaan);

        Scene tseen = new Scene(paigutus);
        primaryStage.setScene(tseen);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
