package Hangman.Core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Leho Kivistik on 22.01.2017.
 */
public class ArvamisMang {

    private List<Sona> arvatavadSonad = new ArrayList<>();
    private List<SonaArvamine> mangitudSonad = new ArrayList<>();
    SonaArvamine sonaArvamine = null;

    public final List<SonaArvamine> mangitudSonad() {
        return this.mangitudSonad;
    }
    public SonaArvamine SonaArvamine() {
        return sonaArvamine;
    }

    /**
     * Initisialiseerib uue mängu ja säilitab kõik mängitud sõnad
     *
     * @param arvatavadSonad Äraarvatavate sõnade nimekiri
     * @param arvatavadTahed Äraarvamisel lubatud tähed (tähestik)
     * @throws Exception
     */
    public ArvamisMang(List<Sona> arvatavadSonad, CharSequence arvatavadTahed) throws Exception {
        String lubatudMargid = arvatavadTahed.toString() + " -";
        for (Sona sona : arvatavadSonad)
            for (Character chr : sona.getKirjapilt().toUpperCase().toCharArray()) {
                if (!lubatudMargid.contains(annaMaping(chr).toString()))
                    throw new Exception("Arvatavad sõnad " + sona.getKirjapilt().toUpperCase() + "  sisaldab lubamatut märki" +
                            chr.toString() + "!");
            }
        this.arvatavadSonad = arvatavadSonad;
    }

    /**
     * Genereerib äraarvatava sõna, jälib et antud sõna ei oleks juba arvatud
     *
     * @param elusid    Kui mitu katset on lubatud
     */
    public void alusta(int elusid) {
        Random randomGenerator = new Random();
        Sona uusSona = new Sona();
        //leiame veel mängimata sõna
        while (mangitudSonad.size() < arvatavadSonad.size()) {
            Sona sona = arvatavadSonad.get(randomGenerator.nextInt(arvatavadSonad.size()));
            if (mangitudSonad.stream().map(c -> c.getArvatavSona()).filter(c -> sona.toString() == c).count() == 0) {
                uusSona = sona;
                break;
            }
        }
        //kas arvatavad sõnad otsas?
        if  (uusSona == null)
            return;
        sonaArvamine = new SonaArvamine(uusSona.toString(), elusid);
        mangitudSonad.add(sonaArvamine);
    }

    private Character annaMaping(Character originaal) {
        if (originaal == 'Ã')
            return 'A';
        if (originaal == 'É')
            return 'E';
        if (originaal == 'Í')
            return 'I';
        return originaal;
    }
}
