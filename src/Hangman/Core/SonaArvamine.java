package Hangman.Core;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Leho Kivistik on 22.01.2017.
 */
public class SonaArvamine {

    private String arvatavSona = new String();
    private StringProperty peidetudSona = new SimpleStringProperty();
    private ObservableList<Character> pakutudTahed = FXCollections.observableArrayList();

    public ObservableList<Character> pakutudTahed() {
        return pakutudTahed;
    }

    private String oigestiArvatudTahed = new String();
    private IntegerProperty valesidPakkumisi = new SimpleIntegerProperty(0);
    /**
     * manguseis 0 = mängu algus; 1 = mäng pooleli; -1 = kaotus; -2 = tubli
     */
    private IntegerProperty manguSeis = new SimpleIntegerProperty(0);
    private int elusid;

    public int elusid() {
        return elusid;
    }

    public String getArvatavSona() {
        return arvatavSona.toUpperCase();
    }

    public String peidetudSona() {
        return peidetudSona.get().toUpperCase();
    }

    public StringProperty peidetudSonaProp() {
        return peidetudSona;
    }

    public IntegerProperty valesidPakkumisiProp() {
        return valesidPakkumisi;
    }

    public IntegerProperty manguSeis() {
        return manguSeis;
    }

    /**
     * Uus sõnaarvamine
     *
     * @param sona   äraarvatav sõna
     * @param elusid lubatud arvamiskatseid
     */
    public SonaArvamine(String sona, int elusid) {
        this.arvatavSona = sona;
        this.elusid = elusid;
        peidetudSona.set(annaPeidetudSona());
    }

    /**
     * Tähe äraarvamise meetod, jälgib valesti pakutud ja õigesti pakutud tähti
     *
     * @param chr äraarvatav täht
     */
    public void pakuTaht(Character chr) {
        if (manguSeis.get() < 0) return;
        chr = chr.toString().toUpperCase().toCharArray()[0];
        pakutudTahed.add(chr);
        if (!getArvatavSona().contains("" + annaMaping(chr)))
            valesidPakkumisi.set(valesidPakkumisi.get() + 1);
        else
            oigestiArvatudTahed = oigestiArvatudTahed + chr.toString();
        if (valesidPakkumisi.get() >= elusid)
            manguSeis.set(-1);
        else
            manguSeis.set(onOigestiArvatud() ? -2 : 1);
        peidetudSona.set(annaPeidetudSona());
    }

    /**
     * @return tagastab äraarvatava sõna selliselt, et veel äraarvamata tähed on asendatud _-ga
     */
    private String annaPeidetudSona() {
        StringBuilder arvatudSona = new StringBuilder(getArvatavSona());
        for (int i = 0; i < getArvatavSona().length(); i++) {
            Character chr = getArvatavSona().charAt(i);
            if (chr == ' ' || chr == '-')
                arvatudSona.setCharAt(i, chr);
            else
                arvatudSona.setCharAt(i, oigestiArvatudTahed.contains(chr.toString()) ? chr : '_');
        }
        return arvatudSona.toString();
    }

    /**
     *
     * @return tagastab true kui sõna on ära arvatud
     */
    private boolean onOigestiArvatud() {
        if (arvatavSona.length() < 1 || pakutudTahed.size() < 1)
            return false;
        if (valesidPakkumisi.get() >= elusid)
            return false;
        for (Character tahtSonast : getArvatavSona().toCharArray()) {
            if (!pakutudTahed.toString().toUpperCase().contains(tahtSonast.toString()) && tahtSonast != ' ' && tahtSonast != '-')
                return false;
        }
        return true;
    }

    private Character annaMaping(Character originaal) {
        if (originaal == 'Ã')
            return 'A';
        if (originaal == 'É')
            return 'E';
        if (originaal == 'Í')
            return 'I';
        return originaal;
    }
}
