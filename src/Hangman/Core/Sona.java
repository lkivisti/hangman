package Hangman.Core;

/**
 * Created by Leho Kivistik on 21.01.2017.
 */
public class Sona {

    private String kirjapilt;
    private Kategooria kategooria;

    public Sona() {}
    public Sona(String kirjapilt, Kategooria kategooria) {
        this.kirjapilt = kirjapilt;
        this.kategooria = kategooria;
    }

    public String getKirjapilt() {
        return kirjapilt;
    }

    public String toString() {
        return getKirjapilt();
    }
}
